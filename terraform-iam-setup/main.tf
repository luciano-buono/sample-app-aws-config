terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
provider "aws" {
  profile             = var.aws_profile
  region              = var.aws_region
  allowed_account_ids = ["${var.aws_account_id}"]
  # assume_role {
  #   role_arn = var.workspace_iam_roles[terraform.workspace]
  # }

  default_tags {
    tags = {
      environment = var.tags_environment
      project     = var.project
      terraform   = true
    }
  }
}

data "tls_certificate" "gitlab" {
  url = var.gitlab_url
}

resource "aws_iam_openid_connect_provider" "gitlab" {
  url            = var.gitlab_url
  client_id_list = [var.aud_value]
  # thumbprint_list = ["${data.tls_certificate.gitlab.certificates.0.sha1_fingerprint}"]
  thumbprint_list = ["41F31E077C7A914F62B5002BD17E7C72A2FB6767", "B3DD7606D2B5A8B4A13771DBECC9EE1CECAFA38A"]
}

data "aws_iam_policy_document" "assume-role-policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.gitlab.arn]
    }
    condition {
      test     = "StringLike"
      variable = "${aws_iam_openid_connect_provider.gitlab.url}:${var.match_field}"
      values   = var.match_value
    }
  }
}

resource "aws_iam_role" "gitlab_ci" {
  name_prefix         = "GitLabCI"
  assume_role_policy  = data.aws_iam_policy_document.assume-role-policy.json
  managed_policy_arns = var.assume_role_arn
}


resource "aws_iam_policy" "create_roles" {
  name        = "CreateEC2-roles"
  description = "Allow to create role and instance profile for ec2 connect with ssm"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "CreateRole",
            "Effect": "Allow",
            "Action": [
                "iam:CreateRole",
                "iam:PassRole",
                "iam:UpdateRole",
                "iam:TagRole",
                "iam:DeleteRole",
                "iam:AttachRolePolicy",
                "iam:DeleteRolePolicy",
                "iam:DetachRolePolicy",
                "iam:GetRole",
                "iam:GetRolePolicy",
                "iam:ListAttachedRolePolicies",
                "iam:ListRolePolicies",
                "iam:PutRolePolicy",
                "iam:UpdateRole",
                "iam:UpdateRoleDescription"
            ],
            "Resource": "arn:aws:iam::*:role/Ssm-session-${var.project_app}"
        },
        {
            "Effect": "Allow",
            "Action": [
                "iam:CreateInstanceProfile",
                "iam:AddRoleToInstanceProfile",
                "iam:ListInstanceProfilesForRole",
                "iam:TagInstanceProfile",
                "iam:GetInstanceProfile",
                "iam:RemoveRoleFromInstanceProfile",
                "iam:DeleteInstanceProfile"
            ],
            "Resource": "arn:aws:iam::*:instance-profile/Ssm-session-${var.project_app}"
        },
        {
            "Sid": "ViewRolesAndPolicies",
            "Effect": "Allow",
            "Action": [
                "iam:GetPolicy",
                "iam:ListRoles"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "create_roles" {
  role       = aws_iam_role.gitlab_ci.name
  policy_arn = aws_iam_policy.create_roles.arn
}

output "ROLE_ARN" {
  description = "Role that needs to be assumed by GitLab CI"
  value       = aws_iam_role.gitlab_ci.arn
}
