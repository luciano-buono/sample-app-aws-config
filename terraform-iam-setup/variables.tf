
variable "aws_region" {
  description = "test"
  type        = string
}
variable "aws_profile" {
  description = "test"
  type        = string
}
variable "aws_account_id" {
  description = "test"
  type        = string
}
variable "gitlab_url" {
  description = "test"
  type        = string
  default     = "https://gitlab.com"
}

variable "aud_value" {
  description = "test"
  type        = string
  default     = "https://gitlab.com"
}
variable "match_field" {
  description = "test"
  type        = string
  default     = "aud"
}

variable "match_value" {
  description = "test"
  type        = list(any)
}

variable "assume_role_arn" {
  description = "test"
  type        = list(any)
}

variable "tags_environment" {
  description = "test"
  type        = string
}
variable "project" {
  description = "test"
  type        = string
}
variable "project_app" {
  description = "test"
  type        = string
}