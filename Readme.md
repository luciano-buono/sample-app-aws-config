# Template
This template is an example of creation of infrastructe with IaC and deploying samples app into it.


# **Table of contents**
- [Template](#template)
- [**Table of contents**](#table-of-contents)
- [1. Cloud Provider: AWS](#1-cloud-provider-aws)
- [2. CloudFlare](#2-cloudflare)
- [3. IaC: Terraform](#3-iac-terraform)
  - [Resources:](#resources)
  - [TFState and TFLock](#tfstate-and-tflock)
  - [Parameter Store as secrets vault](#parameter-store-as-secrets-vault)
- [4. Connection to AWS: Assume role with identity provider(no need of storing credentials)](#4-connection-to-aws-assume-role-with-identity-providerno-need-of-storing-credentials)
    - [Terraform authorization to the AWS account](#terraform-authorization-to-the-aws-account)
    - [Setup OICD IdP](#setup-oicd-idp)
- [5. Connecting Session manager](#5-connecting-session-manager)
  - [Connecting to the EC2 instance without SSH keys](#connecting-to-the-ec2-instance-without-ssh-keys)
- [6. Copying and starting the containerized apps: Ansible](#6-copying-and-starting-the-containerized-apps-ansible)
    - [aws\_ssm considerations](#aws_ssm-considerations)
  - [Improvement:](#improvement)
    - [Solution:](#solution)
- [7. CI Tool: Gitlab CI/CD (in charge of integrating everything)](#7-ci-tool-gitlab-cicd-in-charge-of-integrating-everything)
    - [Prefefined CICD variables in the repo](#prefefined-cicd-variables-in-the-repo)
- [8. SAST Scan: Gitlab CI template (scans Teraform and Ansible)](#8-sast-scan-gitlab-ci-template-scans-teraform-and-ansible)
- [9. App hosting: Nginx](#9-app-hosting-nginx)
  - [HTTPS SSL](#https-ssl)
- [10. Documentation: MKDocs (link) (One of the two apps deployed is a static website with this docs)](#10-documentation-mkdocs-link-one-of-the-two-apps-deployed-is-a-static-website-with-this-docs)
- [11. Sample app: Simple Cocktail catalog made with React (made by a friend)](#11-sample-app-simple-cocktail-catalog-made-with-react-made-by-a-friend)

---
# 1. Cloud Provider: AWS
We are creating our resources and building our app in Amazon Web Services
# 2. CloudFlare
We are using CNAME reconds to point the the ALB
 - [docs-sample-app-aws.methizul.com.ar](https://docs-sample-app-aws.methizul.com.ar)
 - [web-sample-app-aws.methizul.com.ar](https://web-sample-app-aws.methizul.com.ar)
# 3. IaC: Terraform
The config is divided in two folders `terraform` and `terraform-iam-setup`. 

The second one will make the initial setup of the `aws_iam_openid_connect_provider`
 and `aws_iam_role` in order to give permission to the Gitlab Runner to be authorized.
 This terraform code will run locally using normal credentials.
## Resources:
The `terraform` folder defines the infrastructure using IaC, primarily Terraform code.

- VPC with 2 private and 2 public subtnets
- Autoscaling group (ASG)
- App load balancer (ALB) linked to the ASG
- EC2 instances using Amazon Linux2 server being set up using a Launch Configuration script
- Iam Role for the EC2 instances (for Session Manager)
- ~~Elastic IPs to ensure the same IP in case of shutdown/poweroff~~ (Not needed since we are routing using the ALB)
- ...


Terraform scripts will be run from the CICD Pipeline on the master/dev branch. Never from the local laptop.
In order to execute the Terraform, a CICD pipeline yaml file will be created where it will execute the `terraform apply` only on the defined branches. For the others branches, only a `terraform validate` and `terraform scan` wil be executed.

## TFState and TFLock
Normally we would use S3 as the Tfstate backend with policys to restrict the access anyone except authorized people. We have to remember that the password are stored in this file so we should treat it carefully (in this app we don't have any resources with password)

Also, we would use DynamoDB to manage the TFLock in order to avoid two deploys at the same time.

**For this project** we are using the Gitlab backend that allows you to store and check your state from the repo.

## Parameter Store as secrets vault
In case we would need to pass any secrets to terraform, we would use the Parameter Store (or KMS but it is more expensive) to define them.
Then the object `aws_ssm_parameter` would retrieve it and we avoid storing any password in the repository

---

# 4. Connection to AWS: Assume role with identity provider(no need of storing credentials)
### Terraform authorization to the AWS account
In order not to store persistent credentials on the repository (even if they are securely stored with CICD Vars) we are going to use the OICD Identity Provider.
Will be creating an IAM role where it will give read/write permission to:
 - VPC
 - SG
 - EC2

The CICD pipeline will have access to this role using `aws sts assume-role-with-web-identity` 
### Setup OICD IdP
Since we need to create the OICD, the policy and the role for Gitlab to use, we will need to create this locally using AWS credentials stored in the laptop. This is done in the folder `terraform-iam-setup`


# 5. Connecting Session manager
## Connecting to the EC2 instance without SSH keys
In order not to store persistent keys in the repository(same login as the AWS account access keys), we won't use plain old ssh with private/public key. Instead we will use the SSM Session manager that uses your IAM permissions.  In order to simplify this, we are using https://github.com/elpy1/ssh-over-ssm. How does this work?
 - Previously have applied the necessary roles in order for the SSM tool to have access to the EC2 instance
 1. You connect to ssm and issue a command to create a temporal public key in the `authorized_keys` inside the EC2
 2. Use your IAM permissions to launch an ssh session inside this SSM tunnel using the temporary ssh keys.
 3. The public ssh key is deleted

---
# 6. Copying and starting the containerized apps: Ansible
We are implementing an Ansible playbook that will:
 - login to aws using the assume role with web identity 
 - ask the status of the ASG and wil return the list of instances Ids
 - add them to a dynamic invetory as hosts
 - connect to the instances using `ansible_connection: aws_ssm` to:
    - copy the deploy folder
    - start the docker compose
We are using a custom Docker image built from the Dockerfile to get a container with all the dependencies ready
### aws_ssm considerations
This module allows you to use the session manager to connect. This needs permission to call to a S3 Bucket and some permission more. I need to check exactly what it needs. Right now I had to give SSM Full accesss and S3 Full access to make it work.
## Improvement:
This playbook would run manually so it won't take into account the possible `autoscaling` feature.
If in the future, we add a dynamic trigger for the autoscaler (http requests, cpu usage, etc) then this new instances won't have the app running until the execution of the playbook.
### Solution:
We could use the `launch configuration` to download the `deploy` folder from a S3 bucket and then start the docker compose.


#  7. CI Tool: Gitlab CI/CD (in charge of integrating everything)
### Prefefined CICD variables in the repo
```
acm_certificate_arn
assume_role_arn
aws_region     
aws_account_id 
ROLE_ARN_dev=(arn of the role created locally)
tags_environment
```

--------------

# 8. SAST Scan: Gitlab CI template (scans Teraform and Ansible) 
We are using the free SAST scan provided by Gitlab. this would output a JSON  artifact with all the vulnebilities and warning in the Terraform and Ansible code to check.

---
# 9. App hosting: Nginx
For the app part we will structure it like this:
- Reverse proxy. One Nginx container working a reverse proxy that will proxypass the request to destination 
- Nginx hosting. One container with a website each.

## HTTPS SSL
Since we are using a Application LoadBalancer, we need to attach the certificate and key into this. For that we will upload the files into ACM (CertificateManger). If you are using a wildcard subdomain certificate, it means that it can be used for any subdomains you like. Therefore, you only need to upload it one time for all your app/loadbalancer.
I'm using my own domain delegated on CloudFlare. So I had to do a CNAME RECORD pointing to the ALB for each app (a different subdomain for each one)

With the above configuration we managed to secure the traffic up to the AWS Cloud. We could also encrypt the traffic from the ALB to the EC2s attaching a certificate in the reverse proxy container:
 - same one used in the ALB
 - an internal one with our own CA (making sure that we trust this CA in the apps)
 - not using any. Forward traffic to the port 80 of the instance (less secure)

Finally we have the connection between the reverse proxy and the nginx apps. 
Since these containers are in the same server we will leave this connection unencrypted (HTTP).


---
# 10. Documentation: MKDocs (link) (One of the two apps deployed is a static website with this docs)
Mkdocs is documentation static site generator that uses the markdown files to create a simple docs site that can be uploaded into Github/Gitlab Pages or can be hosted on your own server (what we did here on: [docs.sample-app-aws.methizul.com.ar](docs.sample-app-aws.methizul.com.ar)
)

# 11. Sample app: Simple Cocktail catalog made with React (made by a friend)
The sample application app. This code is in the `sample-app-aws/webpage` folder.
Ideally it should not live in this repo, it woulb the in its own where we create another Pipeline that would: **scan, build and deploy** in the corresponding Ec2 instances