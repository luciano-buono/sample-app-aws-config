variable "assume_role_arn" {
  description = "test"
  type        = list(any)
}


variable "aws_region" {
  description = "test"
  type        = string
}

variable "aws_account_id" {
  description = "test"
  type        = string
}


variable "workspace_iam_roles" {
  type = object({
    dev = string
    prd = string
  })
}

variable "id_token" {
  type = string
}


variable "tags_environment" {
  type = string
}
variable "project" {
  type = string
}

variable "ami" {
  type = string
}
variable "instance_type" {
  type = string
}

variable "acm_certificate_arn" {
  type = string
}