terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  # backend "s3" {
  #   bucket         = var.terraform_tfstate_bucket
  #   key            = var.terraform.tfstate
  #   region         = var.region
  #   encrypt        = true
  #   dynamodb_table = var.terraform_tfstate_dynamodb
  # }
  backend "http" {
  }
}

provider "aws" {
  region              = var.aws_region
  allowed_account_ids = ["${var.aws_account_id}"]
  assume_role_with_web_identity {
    role_arn           = var.workspace_iam_roles["dev"]
    web_identity_token = var.id_token
  }

  default_tags {
    tags = {
      environment = var.tags_environment
      project     = var.project
      terraform   = true
    }
  }
}
#Use this to avoid same name errors
resource "random_id" "id" {
  byte_length = 8
}

##Role por SSM Session manager without ssh key
data "aws_iam_policy_document" "assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ssm_session" {
  #Can't be a random name because we specified a specific name in the Resource
  name                = "Ssm-session-${var.project}"
  assume_role_policy  = data.aws_iam_policy_document.assume-role-policy.json
  managed_policy_arns = var.assume_role_arn
}
resource "aws_iam_instance_profile" "ssm_session" {
  name = "Ssm-session-${var.project}"
  role = aws_iam_role.ssm_session.name
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name                 = var.project
  cidr                 = "10.0.0.0/16"
  azs                  = ["${var.aws_region}c", "${var.aws_region}b"]
  enable_dns_hostnames = true
  enable_dns_support   = true
  private_subnets      = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets       = ["10.0.101.0/24", "10.0.102.0/24"]
}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}


# module "ec2_instance" {
#   source  = "terraform-aws-modules/ec2-instance/aws"
#   version = "~> 3.0"

#   name = "${var.project}-${random_id.id.id}"

#   ami                    = data.aws_ami.amazon-linux-2.id
#   instance_type          = var.instance_type
#   iam_instance_profile = aws_iam_instance_profile.ssm_session.name
#   vpc_security_group_ids = [aws_security_group.allow_http_https.id]
#   subnet_id              = module.vpc.public_subnets[0]

#   tags = {
#     Terraform   = "true"
#     Environment = "dev"
#   }
# }


resource "aws_launch_configuration" "sample-app" {
  name_prefix          = "learn-terraform-aws-asg-"
  image_id             = data.aws_ami.amazon-linux-2.id
  instance_type        = var.instance_type
  user_data            = file("user-data.sh")
  security_groups      = [aws_security_group.sample-app_instance.id]
  iam_instance_profile = aws_iam_instance_profile.ssm_session.name
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "sample-app" {
  name                 = "sample-app"
  min_size             = 0
  max_size             = 3
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.sample-app.name
  vpc_zone_identifier  = module.vpc.public_subnets
  target_group_arns    = [aws_lb_target_group.sample-app.arn]

  tag {
    key                 = "Name"
    value               = var.project
    propagate_at_launch = true
  }
}

resource "aws_lb" "sample-app" {
  name               = "${var.project}-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_http_https.id]
  subnets            = module.vpc.public_subnets
}

resource "aws_lb_listener" "sample-app" {
  load_balancer_arn = aws_lb.sample-app.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.acm_certificate_arn


  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.sample-app.arn
  }
}


resource "random_uuid" "some_uuid" {}

resource "aws_lb_target_group" "sample-app" {
  name     = substr(format("%s-%s", "${var.project}-tg", replace(random_uuid.some_uuid.result, "-", "")), 0, 32)
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.vpc.vpc_id
  lifecycle {
    create_before_destroy = true
  }
}




resource "aws_security_group" "sample-app_instance" {
  name = "learn-asg-sample-app-instance"
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.allow_http_https.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group" "allow_http_https" {
  name        = "allow_tls_http"
  description = "Allow TLS HTTP inbound traffic"
  vpc_id      = module.vpc.vpc_id
  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] #[vpc.main.cidr_block]
    ipv6_cidr_blocks = ["::/0"]      #[vpc.main.ipv6_cidr_block]
  }
  ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] #[vpc.main.cidr_block]
    ipv6_cidr_blocks = ["::/0"]      #[vpc.main.ipv6_cidr_block]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

output "aws_lb_dns" {
  description = "This is the alias to add in your DNS record"
  value       = aws_lb.sample-app.dns_name
}
