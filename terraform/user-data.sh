#!/bin/bash
# sudo amazon-linux-extras enable nginx1
# sudo yum install nginx -y
# sudo systemctl start nginx



sudo yum install -y yum-utils
sudo yum install -y docker
sudo systemctl enable docker
sudo systemctl start docker

sudo curl -SL https://github.com/docker/compose/releases/download/v2.15.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x  /usr/local/bin/docker-compose
sudo usermod -a -G docker ec2-user
sudo usermod -a -G docker ssm-user
id ec2-user
# Reload a Linux user's group assignments to docker w/o logout
newgrp docker
